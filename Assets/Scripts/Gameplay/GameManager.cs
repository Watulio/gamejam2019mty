﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    public static GameManager instance { get { return _instance; } }

    private int nPlayers = 0;
    public Hashtable players = new Hashtable();

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }
    }

    public void addPlayer(int player)
    {
        if (!players.ContainsKey(player))
        {
            players.Add(player, 1);
            nPlayers++;
        }
  
    }

    public void DeletePlayer(int player)
    {
        if (players.ContainsKey(player))
        {
            if ((int)players[player] == 1)
            {
                players.Remove(player);
            }

            nPlayers--;
        }
    }

    public bool readyToPlay()
    {
        return (nPlayers > 1 ? true : false);
    }
}
