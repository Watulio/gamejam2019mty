﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float speed = 5f;
    public PlayerAttack owner;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        Unit target = other.GetComponent<Unit>();

        if (other.GetComponent<PlayerAttack>() != owner)
        {
            if (target != null)
            {
                target.Stunned();
            }
            Destroy(gameObject);
        }

    }


}
