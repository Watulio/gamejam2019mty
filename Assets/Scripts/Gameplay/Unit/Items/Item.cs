﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public bool held = false;
    internal Rigidbody rb;
    ItemSpawnPoint[] spawnPoints;
    public GameObject[] itemModels;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        spawnPoints = FindObjectsOfType<ItemSpawnPoint>();
        Respawn();
    }

    public void Respawn()
    {
        if(spawnPoints.Length > 0)
        {
            chooseItem();
            int target = Random.Range(0, spawnPoints.Length);
            transform.position = spawnPoints[target].transform.position;
        }
    }

    public void chooseItem()
    {
        int target = Random.Range(0, itemModels.Length);
        for(int i = 0; itemModels.Length > i; i++  )
        {
            if (i == target)
                itemModels[i].SetActive(true);
            else
                itemModels[i].SetActive(false);

        }
    }

}
