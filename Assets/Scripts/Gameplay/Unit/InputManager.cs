﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public int playerIndex = 0;

    public float GetAxis(string name)
    {
        return Input.GetAxis("Joy" + playerIndex + "LS" + name);

    }
    public bool GetButtonDown(string name)
    {
        return Input.GetButtonDown("Joy" + playerIndex + name);
    }
    public bool GetButtonUp(string name)
    {
        return Input.GetButtonUp("Joy" + playerIndex + name);
    }
    public bool GetButton(string name)
    {
        return Input.GetButton("Joy" + playerIndex + name);
    }

}
