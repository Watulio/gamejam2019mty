﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    //Stun
    public bool stun = false;
    public float stunTime = 1.4f;
    protected IEnumerator stunStart;

    //Shared Unit Components
    public Animator anim;


    // Start is called before the first frame update
    protected virtual void Start()
    {
        anim = GetComponentInChildren<Animator>();
    }

    public virtual void Stunned() {
        anim.SetBool("Stunned", true);
    }
    public virtual void UnStunned() {
        anim.SetBool("Stunned", false);
    }

    protected IEnumerator stunCountdown()
    {
        yield return new WaitForSeconds(stunTime);
        UnStunned();
    }

}
