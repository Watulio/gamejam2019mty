﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrandaPathPoint : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        GrandpaMovement grandpa = other.GetComponent<GrandpaMovement>();

        if (grandpa != null)
        {
            if(this == grandpa.locations[grandpa.currentDestination])
            {
                grandpa.ChangeDestination();
            }
            
        }
    }
}
