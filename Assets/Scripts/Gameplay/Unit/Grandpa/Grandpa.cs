﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grandpa : Unit
{
    //Components
    GrandpaMovement mov;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        mov = GetComponent<GrandpaMovement>();
    }

    public override void Stunned()
    {
        base.Stunned();
        mov.Stop();
        mov.enabled = false;
        stunStart = stunCountdown();
        StartCoroutine(stunStart);
    }

    public override void UnStunned()
    {
        base.UnStunned();
        mov.enabled = true;
        mov.Continue();
    }

    private void OnTriggerEnter(Collider other)
    {
        Item newItem = other.GetComponent<Item>();

        if (newItem != null)
        {
            if(!newItem.held)
            newItem.Respawn();
        }
    }
}
