﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GrandpaMovement : UnitMovement
{
    public GrandaPathPoint[] locations;
    NavMeshAgent nav;
    public int currentDestination = -1;
    

    // Start is called before the first frame update
    void Start()
    {
        nav = GetComponent<NavMeshAgent>(); 
        ChangeDestination();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Stop()
    {
        nav.isStopped = true;
    }

    public void Continue()
    {
        nav.isStopped = false;
    }

    public void ChangeDestination()
    {
        if (locations.Length > 0)
        {
            int target = Random.Range(0,locations.Length);
            while (target == currentDestination)
                target = Random.Range(0, locations.Length);

            currentDestination = target;

            nav.SetDestination(locations[target].transform.position);
        }
    }
}
