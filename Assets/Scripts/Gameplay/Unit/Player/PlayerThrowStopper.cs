﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerThrowStopper : MonoBehaviour
{
    public Player player;

    public void StopThrow()
    {
        player.StopThrow();
    }
}
