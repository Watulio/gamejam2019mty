﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit
{
    //Components
    public InputManager im;
    PlayerMovement mov;
    public PlayerScore score;
    PlayerAttack attack;
    PlayerItemHolder holder;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        im = GetComponent<InputManager>();
        mov = GetComponent<PlayerMovement>();
        score = GetComponent<PlayerScore>();
        attack = GetComponent<PlayerAttack>();
        
        holder = GetComponent<PlayerItemHolder>();
        holder.player = this;
        stunStart = stunCountdown();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StopThrow()
    {
        anim.SetLayerWeight(1, 0);
    }

    public override void Stunned()
    {
        base.Stunned();
        mov.Stop();
        mov.enabled = false;
        holder.Drop();
        holder.enabled = false;
        attack.enabled = false;
        stunStart = stunCountdown();
        StartCoroutine(stunStart);
    }

    public override void UnStunned()
    {
        base.UnStunned();
        mov.enabled = true;
        attack.enabled = true;
    }

    public void unArm()
    {
        attack.enabled = false;
    }

    public void Arm()
    {
        attack.enabled = true;
    }



}
