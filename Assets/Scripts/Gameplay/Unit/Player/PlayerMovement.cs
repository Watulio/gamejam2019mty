﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : UnitMovement
{
    InputManager im;
    Rigidbody rb;
    public float speed;
    Vector2 direction;
    public float turnSpeed = 0.7f;
    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody>();
        im = GetComponent<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        direction = new Vector2(-im.GetAxis("Y"), im.GetAxis("X"));
        direction = Vector2.ClampMagnitude(direction, 1)*speed;
        
        rb.velocity = new Vector3(direction.x, rb.velocity.y, direction.y);
        anim.SetFloat("Speed", rb.velocity.magnitude/speed);

        if (direction != Vector2.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(rb.velocity.normalized), turnSpeed);
        }
    }

    public override void Stop()
    {
        rb.velocity = new Vector3(0, rb.velocity.y,0);
    }

    
}
