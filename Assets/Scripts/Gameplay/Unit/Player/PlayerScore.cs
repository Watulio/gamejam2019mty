﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScore : MonoBehaviour
{
    public int score = 0;
    public InputManager im;

    public void Start()
    {
        im = GetComponent<InputManager>();
    }

    public void addPoint()
    {
        score += 1;
        GameplayManager.instance.CheckScore(this);
    }
}
