﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerItemHolder : MonoBehaviour
{
    public Item item;
    public Player player;
    public Transform hand;
    PlayerScore score;
    InputManager im;

    private void Start()
    {
        im = GetComponent<InputManager>();
        score = GetComponent<PlayerScore>();
    }

    public void Drop()
    {
        if(item != null)
        {
            item.transform.parent = null;
            player.Arm();
            item.held = false;
            item.rb.isKinematic = false;
            item = null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Item newItem = other.GetComponent<Item>();

        if (newItem != null)
        {
            if (!newItem.held)
            {
                item = newItem;
                newItem.transform.position = hand.position;
                newItem.transform.rotation = hand.rotation;
                item.transform.parent = transform;
                item.held = true;
                item.rb.isKinematic = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        Grandpa grandpa = other.GetComponent<Grandpa>();

        if (grandpa != null && item != null)
        {
            score.addPoint();
            item.Respawn();
            Drop();

        }
    }

}
