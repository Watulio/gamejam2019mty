﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public Transform spawnPoint;
    public Bullet bullet;
    InputManager im;
    Animator anim;
    public float shootDelay = 0.75f;
    public bool canShoot = true;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        im = GetComponent<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(im.GetButtonDown("A") && canShoot)
        {
            anim.SetLayerWeight(1, 1);
            anim.SetTrigger("Shoot");
            Bullet newBullet = (Bullet)Instantiate(bullet, spawnPoint.position, transform.rotation);
            newBullet.GetComponent<Rigidbody>().velocity = transform.forward * newBullet.speed;
            newBullet.owner = this;
            StartCoroutine(shootCooldown());
        }
    }

    IEnumerator shootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(shootDelay);
        canShoot = true;
    }

}
