﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    public int maxScore = 5;
    public GameObject[] spawnPoints;
    public Player[] playerPrefabs;
    public PlayerScore[] scoreReferences;

    private static GameplayManager _instance = null;
    public static GameplayManager instance { get { return _instance; } }

    public Hashtable testPlayers = new Hashtable();

    private void Start()
    {
        //testSpawn();
        SpawnPlayers(GameManager.instance.players);
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }
    }

    public void CheckScore(PlayerScore playerScore)
    {
        if (playerScore.score >= maxScore)
        {
            //victory
            SceneManager.LoadScene("PlayerMenu");
        }
    }

    void testSpawn()
    {
        testPlayers.Add(0, 1);
        testPlayers.Add(1, 1);
        testPlayers.Add(2, 1);
        testPlayers.Add(3, 1);



        SpawnPlayers(testPlayers);
    }

    public void SpawnPlayers(Hashtable players)
    {
        ICollection keys = players.Keys;
        foreach (int key in keys)
        {
            Player player = (Player)Instantiate(playerPrefabs[key], spawnPoints[key].transform.position, spawnPoints[key].transform.rotation);
            player.im.playerIndex = key;
            scoreReferences[key] = player.score;
        }
    }
}
