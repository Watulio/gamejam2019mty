﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIPlayerReadyUp : MonoBehaviour
{
    InputManager im;
    private bool playerReady = false;
    public GameObject Ready;
    public GameObject Wait;
    // Start is called before the first frame update
    void Start()
    {
        im = GetComponent<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (im.GetButtonDown("A") && !playerReady)
        {
            GameManager.instance.addPlayer(im.playerIndex);
            playerReady = true;
            Ready.SetActive(true);
            Wait.SetActive(false);
        }
        if(im.GetButtonDown("B") && playerReady)
        {
            GameManager.instance.DeletePlayer(im.playerIndex);
            playerReady = false;
            Wait.SetActive(true);
            Ready.SetActive(false);
        }
        if (GameManager.instance.readyToPlay() && Input.GetButtonDown("JoyStart"))
            PlayTheGame();
    }

    void PlayTheGame()
    {
        SceneManager.LoadScene("MainScene");
    }
}
