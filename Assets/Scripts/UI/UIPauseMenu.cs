﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIPauseMenu : MonoBehaviour
{
    public GameObject firstSelectedObject;
    public EventSystem es;
    public GameObject menuUI;

    private void Start()
    {
        es = GameObject.Find("EventSystem").GetComponent<EventSystem>();
    }

    void Update()
    {
        if (Input.GetButtonDown("JoyStart"))
        {
            if (menuUI.activeSelf)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }


    public void Resume()
    {
        menuUI.SetActive(false);
        Time.timeScale = 1f;
        es.SetSelectedGameObject(null);
    }

    void Pause()
    {
        menuUI.SetActive(true);
        Time.timeScale = 0f;
        es.SetSelectedGameObject(null);
        es.SetSelectedGameObject(firstSelectedObject);
    }

    public void Quit()
    {
        SceneManager.LoadScene("TitleMenu");
    }
}
